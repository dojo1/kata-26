require_relative 'reverse_long'
require 'test/unit'

class ReverseLongTest < Test::Unit::TestCase
  def test_case_1
    assert_equal 'Hey wollef sroirraw', esrever('Hey fellow warriors')
    assert_equal 'This is a test', esrever('This is a test')
    assert_equal 'This is rehtona test', esrever('This is another test')
    assert_equal 'sodulaS a otisretlaW', esrever('Saludos a Waltersito')
  end
end
