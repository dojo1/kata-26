def esrever(string)
  string.split(" ").each { |word| word.reverse! if word.length >= 5 }.join(" ")
end